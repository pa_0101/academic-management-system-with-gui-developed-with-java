package ams.main;

import ams.model.facade.AMSFacade;
import ams.model.facade.AMSModel;
import ams.view.AMSMainView;
/**
 * @author Paolo Felicelli
 * AMSDriver class with main method to run the application.
 * Creates an instance of the apps main window and the AMS 
 * model.
 */
public class AMSDriver 
{
	public static void main(String[] args) 
	{	
		AMSModel model = new AMSFacade();
		AMSMainView mainView = new AMSMainView(model);
		mainView.setVisible(true);		
	}
}
