package ams.controller;

import ams.model.Course;
import ams.model.facade.AMSModel;
import ams.view.grid.Grid;

/**
 * @author Paolo Felicelli
 * GridController is responsible for interaction 
 * between the grid and the model.
 */
public class GridController 
{
	private AMSModel model;
	@SuppressWarnings("unused")
	private Grid theGrid;
	
	public GridController(Grid theGrid)
	{
		this.theGrid = theGrid;
		model = theGrid.getAmsMainView().getModel();
	}
	
	public Course[] getAllCourses()
	{
		return model.getAllCourses();
	}
}
