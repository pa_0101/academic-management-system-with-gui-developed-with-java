package ams.controller;

import ams.model.Course;
import ams.model.Program;
import ams.model.exception.ProgramException;
import ams.model.facade.AMSModel;
import ams.view.ResetDialog;

/**
 * @author Paolo Felicelli
 * ResetDialogController delegates all ResetDialog functionality
 * over to the AMS model.
 */
public class ResetDialogController 
{
	@SuppressWarnings("unused")
	private ResetDialog resetDialog;
	private AMSModel model;
	
	public ResetDialogController(ResetDialog resetDialog) 
	{
		this.resetDialog = resetDialog;
		model = resetDialog.getAmsMainView().getModel();
	}
	
	public Program getProgram() 
	{
		return model.getProgram();
	}
	
	public Course[] getAllCourses()
	{	
		return model.getAllCourses();		
	}
	
	public void removeCourse(String courseCode) throws ProgramException
	{
		model.removeCourse(courseCode);
	}
}
