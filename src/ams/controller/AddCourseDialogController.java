package ams.controller;

import ams.model.CoreCourse;
import ams.model.Course;
import ams.model.ElectiveCourse;
import ams.model.exception.ProgramException;
import ams.model.facade.AMSModel;
import ams.view.AddCourseDialog;

/**
 * @author Paolo Felicelli
 * AddCourseDialogController is responsible for delegating 
 * add course events to the model.
 */
public class AddCourseDialogController 
{
	@SuppressWarnings("unused")
	private AddCourseDialog addCourseDialog;
	private AMSModel model;

	public AddCourseDialogController(AddCourseDialog addCourseDialog) 
	{
		this.addCourseDialog = addCourseDialog;
		model = addCourseDialog.getAmsMainView().getModel();
	}
	
	public Course[] getAllCourses()
	{	
		return model.getAllCourses();		
	}
	
	/**
	 * addCourse method delegates the user input parameters form
	 * the add course dialog box to the addCourse method in the AMS model
	 */
	public void addCourse(String courseCode, String courseTitle, String[] preReqs, String courseType, int creditPoints)
	{
		if(courseType.equalsIgnoreCase("CORE"))
		{
			try 
			{
				CoreCourse theCourse = new CoreCourse(courseCode, courseTitle, preReqs);
				//delegate to the model
				model.addCourse(theCourse);
			} 
			catch (ProgramException e) 
			{
				e.printStackTrace();
			}
		}
		
		else if(courseType.equalsIgnoreCase("ELECTIVE"))
		{
			try 
			{
				ElectiveCourse theCourse = new ElectiveCourse(courseCode, courseTitle, creditPoints, preReqs);
				//delegate to the model
				model.addCourse(theCourse);
			} 
			catch (ProgramException e) 
			{
				e.printStackTrace();
			}
		}
	}
}
