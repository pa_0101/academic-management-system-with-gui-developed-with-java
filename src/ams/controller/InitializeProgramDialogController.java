package ams.controller;

import ams.model.Program;
import ams.model.facade.AMSModel;
import ams.view.InitializeProgramDialog;

/**
 * @author Paolo Felicelli
 * InitializeProgramDialogController is responsible for delegating 
 * user event to the model.
 */
public class InitializeProgramDialogController 
{
	private AMSModel model;

	public InitializeProgramDialogController(InitializeProgramDialog initializeProgram) 
	{
		model = initializeProgram.getAmsMainView().getModel();
	}
	
	public Program getProgram()
	{	
		return model.getProgram();		
	}
	
	/**
	 * initializeProgram method delegates the user input parameters form
	 * the init program dialog box to the addProgram method in the AMS model
	 */
	public void initializeProgram(String programCode, String programTitle)
	{
		model.addProgram(new Program(programCode, programTitle));
	}

	
}
