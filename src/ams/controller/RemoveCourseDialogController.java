package ams.controller;

import ams.model.Course;
import ams.model.exception.ProgramException;
import ams.model.facade.AMSModel;
import ams.view.RemoveCourseDialog;

/**
 * @author Paolo Felicelli
 * RemoveCourseDialogController is responsible for delegating 
 * remove course events to the model.
 */
public class RemoveCourseDialogController 
{
	@SuppressWarnings("unused")
	private RemoveCourseDialog removeCourseDialog;
	private AMSModel model;

	public RemoveCourseDialogController(RemoveCourseDialog removeCourseDialog) 
	{
		this.removeCourseDialog = removeCourseDialog;
		model = removeCourseDialog.getAmsMainView().getModel();
	}
	
	public Course[] getAllCourses()
	{	
		return model.getAllCourses();		
	}
	
	/**
	 * removeCourse method delegates the user input parameters form
	 * the remove course dialog box to the removeCourse method in the AMS model
	 */
	public void removeCourse(String courseCode, String courseTitle) throws ProgramException
	{
		model.removeCourse(courseCode);
	}
}
