package ams.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import ams.model.facade.AMSModel;
import ams.view.AddCourseDialog;
import ams.view.InitializeProgramDialog;
import ams.view.RemoveCourseDialog;
import ams.view.ResetDialog;
import ams.view.ToolBar;

/**
 * @author Paolo Felicelli
 * ToolBarController implements ActionListener interface.
 * This class processes the action events of the tool bar and
 * delegates the events to each coresponding dialog.
 */
public class ToolBarController implements ActionListener 
{
	private ToolBar toolBar;
	private AMSModel model;
	
	public ToolBarController(ToolBar toolBar) 
	{
		this.toolBar = toolBar;
		model = toolBar.getAmsMainView().getModel();
	}
	
	/**
	 * ActionPerformed is invoked when a menu action is performed. It instantiates
	 * dialog boxes/setsVisible for the user to enter parameters.
	 * Invoked when an action event occurs within the source (component) that 
	 * registered the ActionListener with Component.addActionListener(ActionListener l))
	 */
	@SuppressWarnings("unused")
	@Override
	public void actionPerformed(ActionEvent arg0) 
	{
		if(arg0.getActionCommand().equalsIgnoreCase("Initialize"))
		{
			InitializeProgramDialog myProgDialog = new InitializeProgramDialog(toolBar.getAmsMainView());
			myProgDialog.setVisible(true);
		}
		
		else if(arg0.getActionCommand().equalsIgnoreCase("Reset"))
		{
			ResetDialog resetCourseDialog = new ResetDialog(toolBar.getAmsMainView());
		}
		
		else if(arg0.getActionCommand().equalsIgnoreCase("Add Course"))
		{
			if(model == null || model.getProgram() == null)
			{
				toolBar.showError("You must add a program first");
				return;
			}
			AddCourseDialog myDialog = new AddCourseDialog(toolBar.getAmsMainView());
			myDialog.setVisible(true);
		}
		
		else if(arg0.getActionCommand().equalsIgnoreCase("Remove Course"))
		{
			if(model == null || model.getProgram() == null)
			{
				toolBar.showError("You must add a program first");
				return;
			}
			
			if(model.getProgram().getAllCourses() == null)
			{
				toolBar.showError("No courses to remove");
				return;
			}
			
			RemoveCourseDialog myCourseDialog = new RemoveCourseDialog(toolBar.getAmsMainView());
			myCourseDialog.setVisible(true);
		}
	}
}
