package ams.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import ams.model.facade.AMSModel;
import ams.view.AddCourseDialog;
import ams.view.InitializeProgramDialog;
import ams.view.Menu;
import ams.view.RemoveCourseDialog;
import ams.view.ResetDialog;
/**
 * @author Paolo Felicelli
 * MenuController implements ActionListener interface.
 * This class processes the action events of the menu.
 */
public class MenuController implements ActionListener 
{
	private Menu menu;
	private AMSModel model;

	public MenuController(Menu menu) 
	{
		this.menu = menu;
		model = menu.getAmsMainView().getModel();
	}

	@SuppressWarnings("unused")
	@Override
	/**
	 * ActionPerformed is invoked when a menu action is performed. It instantiates
	 * dialog boxes/setsVisible for the user to enter parameters.
	 * Invoked when an action event occurs within the source (component) that 
	 * registered the ActionListener with Component.addActionListener(ActionListener l))
	 */
	public void actionPerformed(ActionEvent arg0) 
	{
		if(arg0.getActionCommand().equalsIgnoreCase("Initialize")) 
		{
			InitializeProgramDialog myProgDialog = new InitializeProgramDialog(menu.getAmsMainView());
			myProgDialog.setVisible(true);
		}
		
		else if(arg0.getActionCommand().equalsIgnoreCase("Reset")) 
		{
			ResetDialog resetCourseDialog = new ResetDialog(menu.getAmsMainView());
		}

		else if(arg0.getActionCommand().equalsIgnoreCase("Add Course"))
		{
			if(model == null || model.getProgram() == null)
			{
				menu.showError("You must add a program first");
				return;
			}
			
			AddCourseDialog myDialog = new AddCourseDialog(menu.getAmsMainView());
			myDialog.setVisible(true);
		}

		else if(arg0.getActionCommand().equalsIgnoreCase("Remove Course")) 
		{
			if(model == null || model.getProgram() == null)
			{
				menu.showError("You must add a program first");
				return;
			}
			
			if(model.getProgram().getAllCourses() == null)
			{
				menu.showError("No courses to remove");
				return;
			}
			
			RemoveCourseDialog myCourseDialog = new RemoveCourseDialog(menu.getAmsMainView());
			myCourseDialog.setVisible(true);
		}
	}
}
