package ams.controller;

import ams.model.exception.ProgramException;
import ams.model.facade.AMSModel;
import ams.view.grid.GridCell;

/**
 * @author Paolo Felicelli
 * GridCellController is responsible for delegating 
 * remove course events to the model when the user directly
 * manipulates the removal of a course.
 */
public class GridCellController 
{
	private AMSModel model;
	@SuppressWarnings("unused")
	private GridCell gridCell;
	
	public GridCellController(GridCell gridCell)
	{
		this.gridCell = gridCell;
		model = gridCell.getAmsMainView().getModel();
	}
		
	/**
	 * removeCourse method delegates the user mouse event from
	 * the grid to the removeCourse method in the AMS model
	 */
	public void removeCourse(String courseCode) throws ProgramException
	{
		model.removeCourse(courseCode);		
	}
}
