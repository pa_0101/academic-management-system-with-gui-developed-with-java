package ams.model;

/**
 * @author Paolo Felicelli
 * Result class holds the grades of UG/PG students.
 */

public class Result 
{
	private Course course;
	private boolean grade;

	public Result(Course course, boolean grade) 
	{
		this.course = course;
		this.grade = grade;
	}

	public Course getCourse() 
	{
		return course;
	}

	public boolean getGrade() 
	{
		return grade;
	}

	public void setGrade(boolean grade) 
	{
		this.grade = grade;
	}

	/**
	 * String representation of course grades as a pass
	 * or fail
	 */
	public String toString() 
	{
		String gradeString = course.getCode() + ":";
		
		if(grade == true)
		{
			gradeString = gradeString + "PASS";
		}
		
		else
		{
			gradeString = gradeString + "FAIL";
		}
		
		return gradeString;
	}
	
	/**
	 * Equals method compares two objects for equality 
	 * and returns true if they are equal.
	 */
    public boolean equals(Object o) 
	{
		if(o instanceof Result)
		{
			Result object = (Result) o;
			return (course == object.getCourse());
		}
		else
			return false;
	}	
}
