package ams.model;

import ams.model.exception.*;

/**
 * @author Paolo Felicelli
 * University class delegates method operations to the Program 
 * and Student classes
 */

public class University 
{
	private Program program;
	private Student student;
	
	public void addStudent(Student student) 
	{
		this.student = student;
	}
	
	public Student getStudent() 
	{
		return student;
	}
	
	public void addProgram(Program program) 
	{
		this.program = program;
	}

	public Program getProgram() 
	{
		return program;
	}

	public void addCourse(Course course) throws ProgramException 
	{
		program.addCourse(course);
	}
	
	public void removeCourse(String courseId) throws ProgramException 
	{
		program.removeCourse(courseId);
	}

	public Course getCourse(String courseCode) 
	{
		return program.getCourse(courseCode);
	}

	public Course[] getAllCourses() 
	{
		return program.getAllCourses();
	}

	public void enrollIntoCourse(String courseID) throws EnrollmentException 
	{
		student.enrollIntoCourse(getCourse(courseID));
	}
	
	public void withdrawFromCourse(String courseID) throws EnrollmentException 
	{
		student.withdrawFromCourse(getCourse(courseID));
	}
	
	public boolean addResult(Result result) 
	{
		return student.addResult(result);
	}

	public Result[] getResults() 
	{
		return student.getAllResults();
	}
	
	public Course[] getCurrentEnrollment() 
	{
		return student.getCurrentEnrollment();
	}
	
	public int calculateCurrentLoad() 
	{
		return student.calculateCurrentLoad();
	}

	public int calculateCareerPoints() 
	{
		return student.calculateCareerPoints();
	}
	
	public int countCoreCourses() 
	{
		return program.countCoreCourses();
	}

	public int countElectiveCourses() 
	{
		return program.countElectiveCourses();
	}
}
