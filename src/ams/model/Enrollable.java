package ams.model;

import ams.model.exception.EnrollmentException;

/**
 * @author Paolo Felicelli
 * Enrollable interface specifies the common behaviour
 * for UG and PG students.
 */

public interface Enrollable 
{
	public void enrollIntoCourse(Course course) throws EnrollmentException;

	public void withdrawFromCourse(Course course) throws EnrollmentException;
}
