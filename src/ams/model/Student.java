package ams.model;

/**
 * @author Paolo Felicelli
 * Student interface is an extension of the enrollable interface
 * that specifies the common behaviour for UG and PG students.
 */

public interface Student extends Enrollable 
{
	public String getFullName();

	public int getStudentId();
	
	public int calculateCurrentLoad();

	public int calculateCareerPoints();
	
	public boolean addResult(Result result);
	
	public boolean getResult(Course course);

	public Result[] getAllResults();
	
	public Course[] getCurrentEnrollment();

	public String toString();
}
