package ams.model.exception;

/**
 * @author Paolo Felicelli
 * System exception extends provided AMSException
 * as requested.
 */

@SuppressWarnings("serial")
public class ProgramException extends AMSException 
{
	public ProgramException()
	{
		super();
	}

	public ProgramException(String message) 
	{
		super(message);
	}
}
