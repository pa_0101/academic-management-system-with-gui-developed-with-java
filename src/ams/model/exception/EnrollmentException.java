package ams.model.exception;

/**
 * @author Paolo Felicelli
 * System exception extends provided AMSException
 * as requested.
 */

@SuppressWarnings("serial")
public class EnrollmentException extends AMSException 
{
	public EnrollmentException() 
	{
		super();
	}

	public EnrollmentException(String message) 
	{
		super(message);
	}
}
