package ams.model;

import ams.model.exception.EnrollmentException;

/**
 * @author Paolo Felicelli
 * PGStudent class is the concrete subclass of AbstractStudent.
 * Provides concrete implementation for enrolling an PG student.
 */

public class PGStudent extends AbstractStudent 
{
	private final static int MAX_LOAD = 48;
	private final int LOAD_LIMIT = 6;

	public PGStudent(int studentId, String fullName) 
	{
		super(studentId, fullName, MAX_LOAD);
	}

	/**
	 * Method to enroll PG student based whether study load has been 
	 * exceeded and if all preReqs have been passed
	 */
	public void enrollIntoCourse(Course course) throws EnrollmentException 
	{
		boolean isFail = false;
				
		//loop through results, check if there are fails
		for(int i = 0; i < results.size(); i++) 
		{
			//get the result from current element
			Result result = results.get(i);
			//get the grade from the result object
			boolean grade = result.getGrade();
			
			//if the grade is a fail, flag the fail as true
			if(grade == false)
			{
				isFail = true;
			}
		}
		
		//get current load and credit points
		int currLoad = calculateCurrentLoad();
		int credPnts = course.getCreditPoints();
		int totalLoad = currLoad + credPnts;
		
		if((totalLoad > maxLoad) && ((credPnts > (maxLoad + LOAD_LIMIT)) || isFail == true))
		{
			throw new EnrollmentException("PG student has exceeded study load.....");
		}

		String[] preReqs = course.getPreReqs();
		boolean isPassed = false;
		
		if(preReqs != null) 
		{
			//loop through the prereqs
			for(int i = 0; i < preReqs.length; i++) 
			{
				//loop through the results
				for(int j = 0; j < results.size(); j++) 
				{
					//get the preReq from current element
					String preReq = course.getPreReqs()[i];
					//get the result from current element
					String crs = results.get(j).getCourse().getCode();
					//get the grade from current element
					boolean grade = results.get(j).getGrade();
					
					//check grades of prereqs 
					if(crs == preReq && grade == true)
					{
						isPassed = true;
					}
				}
			}
			
			//check if preReqs failed
			if(isPassed == false)
			{
				throw new EnrollmentException("Pre-requisite failed.....");
			}
			
			super.enrollIntoCourse(course);
		} 
		
		else
			super.enrollIntoCourse(course);
	}
}
