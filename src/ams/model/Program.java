package ams.model;

import java.util.*;

import ams.model.exception.*;

/**
 * @author Paolo Felicelli
 * Program class holds all courses related to the 
 * university program.
 */

public class Program 
{
	private String programCode;
	private String programTitle;
	
	//hashMap collection to hold the programs courses
	private HashMap<String, Course> courseMap = new HashMap<String, Course>();
	
	public Program(String programCode, String programTitle) 
	{
		this.programCode = programCode;
		this.programTitle = programTitle;
	}

	public String getProgramCode() 
	{
		return programCode;
	}

	public String getProgramTitle() 
	{
		return programTitle;
	}

	/**
	 * Method to add courses to the program
	 */
	public void addCourse(Course course) throws ProgramException 
	{	
		// put the course into the hashMap
		String code = course.getCode();
        courseMap.put(code, course); 
	}

	/**
	 * Method to remove courses from the program
	 */
	public void removeCourse(String courseCode) throws ProgramException 
	{
		//for each course, return the values
		for(Course course : courseMap.values())
		{	
			//get the pre-reqs
			String[] preReqs = course.getPreReqs();
			
			if(preReqs != null)
			{
				for(int i = 0; i < preReqs.length; i++)
				{
					//don't remove course if pre-req exists
					if(preReqs[i].equalsIgnoreCase(courseCode))
					{
						throw new ProgramException("Pre-requisite course cannot be removed.....");
					}
				}
			}
		}
		
		//remove the course from the hashMap
		courseMap.remove(courseCode);
	}
	
	/**
	 * Get the course that corresponds to the courseCode parameter
	 */
	public Course getCourse(String courseCode) 
	{
		return courseMap.get(courseCode);
	}

	/**
	 * Get the courses from the collection, convert into the Course[] and return
	 */
	public Course[] getAllCourses() 
	{	
		//check if the list contains elements
		if(courseMap.isEmpty())
		{
			return null;
		}
		
		//values() returns collection, converts to Course array for return
		return courseMap.values().toArray(new Course[courseMap.size()]);
	}
	
	/**
	 * Returns string representation of the program
	 */
	public String toString() 
	{
		String s = programCode + ":" + programTitle;
		return s;
	}
	
	/**
	 * Tally up the amount of CORE courses and return tally
	 */
	public int countCoreCourses()
	{
		int total = 0;
		
		// values() returns a view of the values in the map
		for(Course course : courseMap.values())
		{
			String type = course.getCourseType();
			
			if(type.equalsIgnoreCase("CORE"))
			{
				//increment the total
				total++;
			}
		}
		
		return total;
	}

	/**
	 * Tally up the amount of ELECTIVE courses and return tally
	 */
	public int countElectiveCourses() 
	{
		int total = 0;
		
		for(Course course : courseMap.values())
		{
			String type = course.getCourseType();
			
			if(type.equalsIgnoreCase("ELECTIVE"))
			{
				//increment the total
				total++;
			}
		}
		
		return total;
	}
}
