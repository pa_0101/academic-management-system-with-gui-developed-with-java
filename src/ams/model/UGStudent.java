package ams.model;

import ams.model.exception.EnrollmentException;

/**
 * @author Paolo Felicelli
 * UGStudent class is the concrete subclass of AbstractStudent.
 * Provides concrete implementation for enrolling an UG student.
 */
 
public class UGStudent extends AbstractStudent 
{
	private final static int MAX_LOAD = 60;

	public UGStudent(int studentId, String fullName) 
	{
		super(studentId, fullName, MAX_LOAD);
	}

	/**
	 * Method to enroll UG student based whether study load has been 
	 * exceeded and if all preReqs have been passed
	 */
	public void enrollIntoCourse(Course course) throws EnrollmentException 
	{		
		//get current load and credit points
		int currLoad = calculateCurrentLoad();
		int credPnts = course.getCreditPoints();
		int totalLoad = currLoad + credPnts;

		//check if student has exceeded study load, throw exception if exceeded
		if(totalLoad > maxLoad)
		{
			throw new EnrollmentException("UG student has exceeded study load.....");
		}
		
		int cnt = 0;
		
		//get all the preReqs
		String[] currReq = course.getPreReqs();
		
		if(currReq != null) 
		{
			//loop through all preReqs and results
			for(int i = 0; i < currReq.length; i++) 
			{
				for(int j = 0; j < results.size(); j++) 
				{
					//get the preReq from current element
					String preReq = course.getPreReqs()[i];
					//get the result from current element
					String crs = results.get(j).getCourse().getCode();
					//get the grade from current element
					boolean grade = results.get(j).getGrade();
					
					//if the course matches the preReq with pass grade
					if(crs == preReq && grade == true)
					{
						cnt++;
					}
				}
			}
			
			//if passed courses matches preReqs amount
			if(cnt == currReq.length)
			{
				super.enrollIntoCourse(course);
			}
			
			else
				//throw exception if preReq not passed
				throw new EnrollmentException("One of the pre-reqs is not passed.....");
		} 
		
		else
			//enroll student without preReqs if study load not exceeded
			super.enrollIntoCourse(course);
	}
}
