package ams.model;

/**
 * @author Paolo Felicelli
 * AbstractCourse defines common methods relating 
 * core and elective courses.
 */

public abstract class AbstractCourse implements Course 
{
	private String code;
	private String title;
	private int creditPoints;
	private String[] preReqs;

	public AbstractCourse(String code, String title, int creditPoints, String[] preReqs)
	{
		this.code = code;
		this.title = title;
		this.creditPoints = creditPoints;
		this.preReqs = preReqs;
	}

	public String getCode() 
	{
		return code;
	}

	public String getTitle() 
	{
		return title;
	}

	public int getCreditPoints() 
	{
		return creditPoints;
	}

	public String[] getPreReqs() 
	{
		return preReqs;
	}

	/**
	 * Return string representation of courses with or without
	 * pre-reqs
	 */
	public String toString() 
	{
		String[] crsPreReq = getPreReqs();
		String preReqsConcat = "";

		if(crsPreReq != null && crsPreReq.length > 0) 
		{
			for(int i = 0; i < crsPreReq.length; i++) 
			{
				//concatenate pre-req
				preReqsConcat = preReqsConcat + crsPreReq[i];
			}
			
			//if pre-reqs exist
			return code + ":" + title + ":" + creditPoints + ":" + preReqsConcat;
		} 
		
		else
			// if no pre-reqs exist
			return code + ":" + title + ":" + creditPoints;
	}

	/**
	 * Abstract getter that returns the COURSE_TYPE string
	 */
	public abstract String getCourseType();
	
}
