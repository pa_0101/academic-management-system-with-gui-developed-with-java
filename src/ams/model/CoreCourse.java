package ams.model;

/**
 * @author Paolo Felicelli
 * CoreCourse class is concrete subclass of AbstractCourse.
 * Contains concrete implementations of methods relating to 
 * core courses.
 */

public class CoreCourse extends AbstractCourse 
{
	private final static int CREDIT_POINTS = 12;
	private final String COURSE_TYPE = "CORE";

	public CoreCourse(String code, String title, String [] preReqs) 
	{
		super(code, title, CREDIT_POINTS, preReqs);
	}
	
	/**
	 * Return string representation of the core course
	 */
	public String toString() 
	{
		return super.toString() + ":" + COURSE_TYPE;
	}
	
	/**
	 * Getter that returns the COURSE_TYPE string
	 */
	public String getCourseType()
	{
		return COURSE_TYPE;
	}
}
