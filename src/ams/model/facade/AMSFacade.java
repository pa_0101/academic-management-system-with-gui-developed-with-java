package ams.model.facade;

import ams.model.*;

import ams.model.exception.*;

/**
 * @author Mikhail Perepletchikov
 */

/*
 * Facade class delegates all method operations from the model 
 * to the University class
 */

public class AMSFacade implements AMSModel 
{
	private University university;

	public AMSFacade() 
	{
		university = new University();
	}
	
	public void addStudent(Student newStudent) 
	{
		university.addStudent(newStudent);
	}

	public Student getStudent() 
	{
		return university.getStudent();
	}

	public void addProgram(Program program) 
	{
		university.addProgram(program);
	}
	
	public Program getProgram() 
	{
		return university.getProgram();
	}
	
	public void addCourse(Course course) throws ProgramException 
	{
		university.addCourse(course);
	}

	public void removeCourse(String courseId) throws ProgramException 
	{
		university.removeCourse(courseId);
	}

	public Course getCourse(String courseCode) 
	{
		return university.getCourse(courseCode);
	}

	public Course[] getAllCourses() 
	{
		return university.getAllCourses();
	}
	
	public void enrollIntoCourse(String courseID) throws EnrollmentException 
	{
		university.enrollIntoCourse(courseID);
	}

	public void withdrawFromCourse(String courseID) throws EnrollmentException 
	{
		university.withdrawFromCourse(courseID);
	}

	public boolean addResult(Result result) 
	{
		return university.addResult(result);
	}

	public Result[] getResults() 
	{
		return university.getResults();
	}

	public Course[] getCurrentEnrollment() 
	{
		return university.getCurrentEnrollment();
	}

	public int calculateCurrentLoad() 
	{
		return university.calculateCurrentLoad();
	}

	public int calculateCareerPoints() 
	{
		return university.calculateCareerPoints();
	}

	public int countCoreCourses() 
	{
		return university.countCoreCourses();
	}

	public int countElectiveCourses() 
	{
		return university.countElectiveCourses();
	}
}