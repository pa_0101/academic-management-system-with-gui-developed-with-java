package ams.model;

/**
 * @author Paolo Felicelli
 * ElectiveCourse class is a concrete subclass of AbstractCourse.
 * Contains concrete implementations of methods relating to 
 * Elective courses.
 */

public class ElectiveCourse extends AbstractCourse 
{
	private final static int CREDIT_POINTS = 6;
	private final String COURSE_TYPE = "ELECTIVE";

	public ElectiveCourse(String code, String title, String [] preReqs) 
	{
		super(code, title, CREDIT_POINTS, preReqs);
	}

	public ElectiveCourse(String code, String title, int creditPoints, String[] preReqs) 
	{
		super(code, title, creditPoints, preReqs);
	}
	
	/**
	 * Return string representation of the elective course
	 */
	public String toString() 
	{
		return super.toString() + ":" + COURSE_TYPE;
	}
	
	/**
	 * Getter that returns the COURSE_TYPE string
	 */
	public String getCourseType()
	{
		return COURSE_TYPE;
	}
}
