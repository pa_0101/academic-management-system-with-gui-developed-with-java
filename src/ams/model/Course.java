package ams.model;

/**
 * @author Paolo Felicelli
 * Course interface specifies the common behaviour
 * for elective and core courses.
 */

public interface Course 
{
	public String getCode();

	public String getTitle();

	public int getCreditPoints();

	public String[] getPreReqs();
	
	public String toString();
	
	public abstract String getCourseType();	
}
