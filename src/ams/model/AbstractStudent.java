package ams.model;

import java.util.*;

import ams.model.exception.EnrollmentException;

/**
* @author Paolo Felicelli
* AbstractStudent defines common methods relating 
* UG and PG students
*/

public abstract class AbstractStudent implements Student 
{
	private String fullName;
	private int studentId;
	protected int maxLoad;
	
	//arrayList collections that hold the enrollments and results
	private List<Course> enrollments = new ArrayList<Course>();
	protected List<Result> results = new ArrayList<Result>();
	
	public AbstractStudent(int studentId, String fullName, int maxLoad) 
	{
		this.fullName = fullName;
		this.studentId = studentId;
		this.maxLoad = maxLoad;
	}

	public String getFullName() 
	{
		return fullName;
	}

	public int getStudentId() 
	{
		return studentId;
	}

	/**
	 * Method to calculate study load for currently enrolled courses
	 */
	public int calculateCurrentLoad() 
	{
		int totalLoad = 0;
		
		for (int i = 0; i < enrollments.size(); i++) 
		{
			//get each course element 
			Course currCourse = enrollments.get(i);
			//get points from each object
			int currentLoad = currCourse.getCreditPoints();
			//add credit points
			totalLoad = totalLoad + currentLoad;
		}
		return totalLoad;
	}

	/**
	 * Method to calculate the number of credit points for passed courses
	 */
	public int calculateCareerPoints() 
	{
		int totalPoints = 0;
		
		//loop through the results collection
		for(int i = 0; i < results.size(); i++) 
		{
			//get the result from current element
			Result currResult = results.get(i);
			//get the grade
			boolean currGrade = currResult.getGrade();
			//get the course
			Course currCourse = currResult.getCourse();

			//get the credit points 
			int crPnts = currCourse.getCreditPoints();
			
			//if the student has pass grade, add credit points to total points
			if(currGrade == true) 
			{
				totalPoints = totalPoints + crPnts;
			}
		}
		return totalPoints;
	}
	
	/**
	 * Method to add results for completed courses
	 */
	public boolean addResult(Result result) 
	{
		//get the course from the result object
		Course currCourse = result.getCourse();

		//check if enrollment list contains the current course
		if(enrollments.contains(currCourse)) 
		{
			if(results.contains(result))
			{
				results.remove(result);
			}
			
			//add the result to the results list
			results.add(result);
			//get the course associated with the result
			//and remove it from the enrollments list
			enrollments.remove(result.getCourse());
			return true;
		}
		return false;
	}
	
	/**
	 * Get the result if there is one from a previous course
	 */
	public boolean getResult(Course course) 
	{		
		for(int i = 0; i < results.size(); i++) 
		{
			//get result from current element
			Result currResult = results.get(i);
			
			if(currResult.getCourse().equals(course))
			{
				return currResult.getGrade();
			}
		}
		return false;
	}

	/**
	 * Get the results from the list, convert to array and return
	 */
	public Result[] getAllResults() 
	{
		//check if the list contains elements
		if(results.isEmpty())
	    {
			return null;
	    }
		
		//convert list to Result array and return
		System.out.println(results.contains(results));
	    return results.toArray(new Result[results.size()]);
	}
	
	/**
	 * Get the courses from the list, convert into the Course[] and return 
	 */
	public Course[] getCurrentEnrollment() 
	{
		//check if the list contains elements
		if(enrollments.isEmpty())
		{
			return null;
		}
		
		//convert list to Course array and return
		return enrollments.toArray(new Course[enrollments.size()]);
	}
	
	/**
	 * String representation of a student
	 */
	public String toString() 
	{
		String studentString = studentId + ":" + fullName + ":" + maxLoad;
		return studentString;
	}
	
	/**
	 * Method to enroll student into courses
	 */
	public void enrollIntoCourse(Course course) throws EnrollmentException 
	{
		//check if student has already enrolled or previously done/passed a course
		if(enrollments.contains(course) || getResult(course))
		{
			throw new EnrollmentException("Course already listed or previously passed.....");
		}
		
		//add the course to the students enrollment list
		enrollments.add(course);
	}
	
	/**
	 * Method to withdraw from course
	 */
	public void withdrawFromCourse(Course course) throws EnrollmentException 
	{
		//flag for enrollment containing a course
		boolean isFound = false;
		
		for(int i = 0; i < enrollments.size(); i++)
		{
			if(enrollments.contains(course))
			{
				//flag to yes(true) if list contains a course
				isFound = true;
				break;
			}
		}
			
		if(isFound)
		{
			//remove a course if it exists
			enrollments.remove(course);
		}
		
		else
			throw new EnrollmentException("Cannot withdraw from course.....");
	}
}
