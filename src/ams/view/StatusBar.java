package ams.view;

import java.awt.Color;
import javax.swing.*;

/**
 * @author Paolo Felicelli
 * StatusBar class displays current info of initialised program
 * and the amount of core/elective courses int the program.
 * StatusBar is refreshed dynamically as courses and programs are
 * initialized.
 */
@SuppressWarnings("serial")
public class StatusBar extends JPanel 
{
	@SuppressWarnings("unused")
	private AMSMainView amsMainView;
	private JLabel programString;
		
	public StatusBar(AMSMainView amsMainView) 
	{
		this.amsMainView = amsMainView;
		setBackground(Color.WHITE);
		programString = new JLabel("Program: ");
		add(programString);
	}

	/**
	 * update() method is called form the main AMSMainView class
	 * to display program/course info from the model using the
	 * setText method from the API.
	 */
	public void update(String programInfo) 
	{
		programString.setText(programInfo);
	}		
}
