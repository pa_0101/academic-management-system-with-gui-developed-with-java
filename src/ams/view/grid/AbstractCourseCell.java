package ams.view.grid;

import java.awt.Color;
import java.util.Arrays;

import ams.model.Course;
import ams.model.ElectiveCourse;

/**
 * @author Paolo Felicelli
 * AbstractCourseCell defines common display behaviour 
 * for both core and elective courses.
 */
@SuppressWarnings("serial")
public abstract class AbstractCourseCell extends GridCell 
{
	private Course course;
	
	public AbstractCourseCell(Grid grid)
	{
		super(grid);
		setBackground(Color.GRAY);
	}
	
	public void setCourse(Course course)
	{
		this.course = course;
	}
	
	/**
	 * refresh() paints the course information when a
	 * new core/elective course cell is added
	 */
	public void refresh()
	{
		String courseCode = course.getCode();
		String courseTitle = course.getTitle();
		int creditPoints = course.getCreditPoints();
		String[] preReqs = course.getPreReqs();
		@SuppressWarnings("unused")
		String courseType = "CORE";
		String none = "none";
		
		if(course instanceof ElectiveCourse)
		{
			courseType = "ELECTIVE";
		}
		
		if(preReqs == null)
		{
			setText("<html>Code: " + courseCode + "<br>Title: " + courseTitle + "<br>Credit Points: " + 
					creditPoints + "<br>PreReqs: " + none + "</html>");
		}
		
		else
		{
			setText("<html>Code: " + courseCode + "<br>Title: " + courseTitle + "<br>Credit Points: " + 
					creditPoints + "<br>PreReqs: " + Arrays.toString(preReqs) + "</html>");
		}
		
		repaint();
	}
	
	public Course getCourse()
	{
		return course;
	}
}
