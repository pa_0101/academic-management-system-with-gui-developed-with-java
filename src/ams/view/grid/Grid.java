package ams.view.grid;

import java.awt.Color;
import java.awt.GridLayout;

import javax.swing.JPanel;
import javax.swing.JScrollPane;

import ams.controller.GridController;
import ams.model.*;
import ams.view.AMSMainView;

/**
 * @author Paolo Felicelli
 * Grid class is responsible for holding newly created courses.
 * It dynamically refreshes with updated course info as courses 
 * are added or removed.
 */
@SuppressWarnings("serial")
public class Grid extends JPanel
{
	private AMSMainView amsMainView;
	private GridController gridController;
	
	public Grid(AMSMainView amsMainView)
	{
		this.amsMainView = amsMainView;
		gridController = new GridController(this);
		setLayout(new GridLayout(0,4));
	    setBackground(Color.WHITE);				
	}
	
	public void refresh()
	{
		//get all the courses
		Course[] courses = gridController.getAllCourses();
		
		//removeAll() removes all the course elements, so the grid
		//can be refreshed with the new amount and sequence of courses
		removeAll();
		
		if(courses != null)
		{
			for(int i = 0; i < courses.length; i++)
			{
				//course at the current element in the list
				Course currCourse = courses[i];
				
				//if the current element(course) is a core course
				if(currCourse instanceof CoreCourse)
				{
					//instantiate a CoreCourseCell
					CoreCourseCell myCell = new CoreCourseCell(this);	
					//set the current core course to the core course cell
					myCell.setCourse(currCourse);
					//call refresh() in the AbstractCourseCell class
					//to set course parameters as text for the coresponding course
					myCell.refresh();
					//instantiate a JScrollPane that creates vert/hori scroll
					//bars to allow for longer strings of text in the cell
					JScrollPane scrollPane = new JScrollPane(myCell);
					add(scrollPane);
				}
				
				//if the current element(course) is a elective course
				else if(currCourse instanceof ElectiveCourse)
				{
					//instantiate a ElectiveCourseCell
					ElectiveCourseCell myCell = new ElectiveCourseCell(this);
					//set the current elective course to the core course cell
					myCell.setCourse(currCourse);
					//call refresh() in the AbstractCourseCell class
					//to set course parameters as text for the coresponding course
					myCell.refresh();
					//instantiate a JScrollPane that creates vert/hori scroll
					//bars to allow for longer strings of text in the cell
					JScrollPane scrollPane = new JScrollPane(myCell);
					add(scrollPane);
				}
			}
		
			/**
			 * Algorithm for laying out course cells in to rows of 4.
			 * Starts a new row once a row is filled with 4 courses
			 */
			//numOfRows increments by 1 when the row is filled with 4 courses
			int numOfRows = courses.length / 4;
			//blankCells decrements as new course cells are added
			int blankCells = 4 - (courses.length - (numOfRows * 4));
			
			if(blankCells < 4)
			{			
				for(int i = 0; i < blankCells; i++)
				{
					add(new GridCell(this));
				}
			}
		}
		
		//repaint() is called to re-display the grid each time courses are added
		repaint();
	}
	
	public AMSMainView getAmsMainView() 
	{
		return amsMainView;
	}
}
