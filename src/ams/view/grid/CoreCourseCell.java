package ams.view.grid;

import java.awt.Color;

import javax.swing.border.LineBorder;

/**
 * @author Paolo Felicelli
 * CoreCourseCell class is concrete subclass of AbstractCourseCell.
 * Extends for a customized display for a core course.
 */
@SuppressWarnings("serial")
public class CoreCourseCell extends AbstractCourseCell 
{
	public CoreCourseCell(Grid grid)
	{
		super(grid);
		setBorder(new LineBorder(Color.BLACK, 10));
	}
}
