package ams.view.grid;

import javax.swing.border.EmptyBorder;

/**
 * @author Paolo Felicelli
 * ElectiveCourseCell class is concrete subclass of AbstractCourseCell.
 * Extends for a customized display for an elective course.
 */
@SuppressWarnings("serial")
public class ElectiveCourseCell extends AbstractCourseCell 
{
	public ElectiveCourseCell(Grid grid)
	{
		super(grid);
		setBorder(new EmptyBorder(10, 10, 10, 10));
	}
}
