package ams.view.grid;

import java.awt.Color;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import ams.controller.GridCellController;
import ams.model.exception.ProgramException;
import ams.view.AMSMainView;

/**
 * @author Paolo Felicelli
 * GridCell provides the functionality for direct manipulation 
 * of course removal events. Events are delegated to the controller
 * to be passed to the model.
 */

@SuppressWarnings("serial")
public class GridCell extends JPanel implements MouseListener
{
	@SuppressWarnings("unused")
	private AMSMainView amsMainView;
	private Grid grid;
	private JLabel labelText;
	private GridCellController myController;
	
	public GridCell(Grid grid)
	{
		this.grid = grid;
		myController = new GridCellController(this);
		labelText = new JLabel();		
		setBackground(Color.WHITE);
		add(labelText);
		addMouseListener(this);
	}
	
	public void setText(String theText)
	{
		labelText.setText(theText);
		repaint();
	}

	public AMSMainView getAmsMainView() 
	{
		return grid.getAmsMainView();
	}
	
	public void setGrid(Grid grid)
	{
		this.grid = grid;
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub
		
	}
	
	/**
	 * mousePressed is invoked when mouse button has been pressed on
	 * a course cell. It displays a JOptionPane to confirm whether
	 * the user is sure about removing a course. Events are then delegated to the
	 * GridCellController.
	 */
	@Override
	public void mousePressed(MouseEvent arg0) 
	{
		Object myObject = arg0.getSource();

		if(myObject instanceof CoreCourseCell)
		{
			CoreCourseCell myCell = (CoreCourseCell)myObject;
			String courseID = myCell.getCourse().getCode();
			
			if(JOptionPane.showConfirmDialog(null, "Are you sure?", "Remove Core Course", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
			{
				try 
				{
					myController.removeCourse(courseID);
				} 
				catch (ProgramException e) 
				{					
					JOptionPane.showMessageDialog(null,"Unable to remove course, check it is not a pre requisite");					
				}
			}
			getAmsMainView().refreshAll();
		}
		
		else if(myObject instanceof ElectiveCourseCell)
		{
			ElectiveCourseCell myCell = (ElectiveCourseCell)myObject;
			String courseID = myCell.getCourse().getCode();
			
			if(JOptionPane.showConfirmDialog(null, "Are you sure?", "Remove Elective Course", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
			{
				try 
				{
					myController.removeCourse(courseID);
				} 
				catch (ProgramException e) {
					JOptionPane.showMessageDialog(null,"Unable to remove course, check pre requisites");
				}
			}
			getAmsMainView().refreshAll();		
		}
	}
}
