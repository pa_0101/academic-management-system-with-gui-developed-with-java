package ams.view;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import ams.controller.RemoveCourseDialogController;
import ams.model.Course;
import ams.model.exception.ProgramException;

/**
* @author Paolo Felicelli
* RemoveCourseDialog class is a popup dialog that displays a
* list of course info that can be selected and removed.
*/
@SuppressWarnings("serial")
public class RemoveCourseDialog extends JDialog implements ActionListener
{
		@SuppressWarnings("rawtypes")
		private JList courseList;
		private AMSMainView amsMainView;
		private JButton removeCourse;
		private RemoveCourseDialogController removeCourseDialogController; 
		
		@SuppressWarnings({ "rawtypes", "unchecked" })
		public RemoveCourseDialog(AMSMainView amsMainView)
		{			
			this.amsMainView = amsMainView;
			removeCourseDialogController = new RemoveCourseDialogController(this);
			setTitle("Remove Course");
			setSize(350, 250);
			
			//APPLICATION_MODAL blocks all input to the top level window
			setModalityType(ModalityType.APPLICATION_MODAL);
			
			//set the layout of the dialog box
			GridLayout myGridLayout = new GridLayout(); 
			myGridLayout.setColumns(2);
			myGridLayout.setRows(2);
			
			//dialog button
			removeCourse = new JButton("Remove Course");
			
			//set course panel layout
			JPanel coursePanel = new JPanel();
			coursePanel.setLayout(new BorderLayout());
			
			//set main panel layout and border size
			JPanel mainPanel = new JPanel();
			mainPanel.setBorder(new EmptyBorder(10, 10, 10, 10));
			
			//DefaultListModel delegates to an array list, also provides 
			//similar methods of and array list
			DefaultListModel listModel = new DefaultListModel(); 
			
			//get all the courses in the program
			Course[] myCourses = removeCourseDialogController.getAllCourses();
			
			if(myCourses != null)
			{		
				for(int i = 0; i < myCourses.length; i++)
				{
					String courseCode = myCourses[i].getCode();
					String courseTitle = myCourses[i].getTitle();
					//add course to the end of the DefaultListModel
					listModel.addElement(courseCode + " - " + courseTitle);
				}
			}
			
			//set the mode of selecting a course from the list
			courseList = new JList(listModel);
			courseList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			JScrollPane scrollPane = new JScrollPane(courseList);
			
			//add components and set layout for main panel 
			mainPanel.setLayout(myGridLayout);
			mainPanel.add(scrollPane);
			mainPanel.add(removeCourse);
			removeCourse.addActionListener(this);
			add(mainPanel);
			setLocationRelativeTo(this);
			
			//registerKeyboardAction allows user to interact with JButton using enter/spacebar key
			removeCourse.registerKeyboardAction(removeCourse.getActionForKeyStroke(
	                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
	                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
	                JComponent.WHEN_FOCUSED);
			removeCourse.registerKeyboardAction(removeCourse.getActionForKeyStroke(
	                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
	                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
	                JComponent.WHEN_FOCUSED);
		}
		
		/**
		 * ActionPerformed is invoked when remove course button is pressed.
		 * Grabs the text input and passes it to initializeProgram as parameters.
		 * Also invokes refreshAll to update the grid 
		 */
		public void actionPerformed(ActionEvent arg0) 
		{
			if(arg0.getActionCommand().equalsIgnoreCase("Remove Course"))
			{
				//check if a course is selected
				if(courseList.getSelectedValuesList().size() == 0)
				{
					JOptionPane.showMessageDialog(this, "No course selected");
					return;
				}
				
				String selectedCourse = courseList.getSelectedValue().toString();
				String[] courseCodeParts = selectedCourse.split(" - ");
				String courseCode = courseCodeParts[0];
				String courseTitle = courseCodeParts[1];
				
				if(JOptionPane.showConfirmDialog(null, "Are you sure?", "Remove Course", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION)
				{
					try 
					{
						removeCourseDialogController.removeCourse(courseCode, courseTitle);
					} 
					catch (ProgramException e) 
					{
						JOptionPane.showMessageDialog(this, "Unable to remove course, check it is not a pre requisite");
					}
					
					setVisible(false);
					//refreshAll() call to update the grid
					amsMainView.refreshAll();
					/**
					 * dispose() causes the JDialog window to be destroyed 
					 * and cleaned up by the operating system
					 */
					dispose();
				}				
			}
		}
		
		public AMSMainView getAmsMainView() 
		{
			return amsMainView;
		}
}


