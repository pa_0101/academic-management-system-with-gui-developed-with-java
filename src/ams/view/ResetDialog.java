package ams.view;

import javax.swing.JOptionPane;

import ams.controller.ResetDialogController;
import ams.model.Course;
import ams.model.exception.ProgramException;
import ams.model.facade.AMSFacade;
import ams.model.facade.AMSModel;

/**
 * @author Paolo Felicelli
 * ResetDialog provides reset functionality via JOption panes
 * to remove all courses from the program while maintaining 
 * the program details. Calls refreshAll function in AMSMainView
 * to update statusBar/grid once courses are removed. 
 */
public class ResetDialog 
{
	private AMSMainView amsMainView;
	@SuppressWarnings("unused")
	private AMSModel model;
	private ResetDialogController resetDialogController;
	
	public ResetDialog(AMSMainView amsMainView)
	{
		this.amsMainView = amsMainView;
		resetDialogController = new ResetDialogController(this);
		model = new AMSFacade();
		
		//reset check, if there is no program initialized to begin with 
		if(resetDialogController.getProgram() == null)
		{
			showError("No program to reset");
			return;
		}
				
		Course[] crs = resetDialogController.getAllCourses();
		
		//reset check, if the program actually has courses added to it
		if(crs != null)
		{
			int result = JOptionPane.showConfirmDialog(null,
		            "Are you sure you want to reset program?",
		            "Reset Program",
		            JOptionPane.YES_NO_OPTION);
			
			if(result == JOptionPane.YES_OPTION)
			{
				for(int i = 0; i < crs.length; i++)
				{
					String crsCode = crs[i].getCode();
					
					try 
					{
						resetDialogController.removeCourse(crsCode);
					} 
					catch (ProgramException e) 
					{
						e.printStackTrace();
					}
				}
			}
			
			//refreshAll() call to update the grid
			amsMainView.refreshAll();
		}
		
		//reset check, if there are no courses added to the program
		else if(resetDialogController.getAllCourses() == null)
		{
			showError("No courses to remove");
			return;
		}
	}

	public AMSMainView getAmsMainView() 
	{
		return amsMainView;
	}
	
	public void showError(String theMessage)
	{
		JOptionPane.showMessageDialog(null,theMessage);
		return;
	}
}
