package ams.view;

import java.awt.event.KeyEvent;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import ams.controller.MenuController;

/**
 * @author Paolo Felicelli
 * Menu class is a control set for the main fuctions:-
 * initialize, reset, add course, remove course.
 */
@SuppressWarnings("serial")
public class Menu extends JMenuBar
{
	private AMSMainView amsMainView;
	private MenuController menuController;
	
	public Menu(AMSMainView amsMainView)
	{
		this.amsMainView = amsMainView;
		menuController = new MenuController(this);
	    JMenu menuFile = new JMenu("Edit");
	    JMenuItem addProgram = new JMenuItem("Initialize");
	    JMenuItem resetProgram = new JMenuItem("Reset");
	    JMenuItem addCourse = new JMenuItem("Add Course");
	    JMenuItem removeCourse = new JMenuItem("Remove Course");
	    
	    //set mnemonic allows user to interact with menu items using enter key
	    menuFile.setMnemonic(KeyEvent.VK_E);

	    menuFile.add(addProgram);
	    menuFile.add(resetProgram);
	    menuFile.add(addCourse);
	    menuFile.add(removeCourse);
	    add(menuFile);
	    
	    /**
	     * Register the JButtons with action listener.
	     * pass the event object to the event listener in 
	     * menuController class
	     */
	    addProgram.addActionListener(menuController);
	    resetProgram.addActionListener(menuController);
		addCourse.addActionListener(menuController);
		removeCourse.addActionListener(menuController);
	}
	
	/**
	 * Method that pops up dialog box that notifies the
	 * user about error conditions.
	 */
	public void showError(String theMessage)
	{
		JOptionPane.showMessageDialog(null,theMessage);
		return;
	}
	
	public AMSMainView getAmsMainView() 
	{
		return amsMainView;
	}
}
