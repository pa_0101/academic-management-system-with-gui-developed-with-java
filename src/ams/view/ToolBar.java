package ams.view;

import java.awt.Color;
import java.awt.event.KeyEvent;

import javax.swing.*;

import ams.controller.ToolBarController;

/**
 * @author Paolo Felicelli
 * ToolBar class is a control set for the main fuctions:-
 * initialize, reset, add course, remove course.
 */
@SuppressWarnings("serial")
public class ToolBar extends JPanel 
{
	private AMSMainView amsMainView;
	private JButton addProgram;
	private JButton resetProgram;
	private JButton addCourse;
	private JButton removeCourse;
	private ToolBarController controller;
	
	public ToolBar(AMSMainView amsMainView) 
	{
		this.amsMainView = amsMainView;
		controller = new ToolBarController(this);
		setBackground(Color.BLACK);
		
		addProgram = new JButton("Initialize");
		add(addProgram);
		resetProgram = new JButton("Reset");
		add(resetProgram);
		addCourse = new JButton("Add Course");
		add(addCourse);
		removeCourse = new JButton("Remove Course");
		add(removeCourse);
		
		//registerKeyboardAction allows user to interact with JButton using enter/spacebar key
		addProgram.registerKeyboardAction(addProgram.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		addProgram.registerKeyboardAction(addProgram.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
		resetProgram.registerKeyboardAction(resetProgram.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		resetProgram.registerKeyboardAction(resetProgram.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
		addCourse.registerKeyboardAction(addCourse.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		addCourse.registerKeyboardAction(addCourse.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
		removeCourse.registerKeyboardAction(removeCourse.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		removeCourse.registerKeyboardAction(removeCourse.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
		
		addProgram.addActionListener(controller);
		resetProgram.addActionListener(controller);
		addCourse.addActionListener(controller);
		removeCourse.addActionListener(controller);
	}
	
	/**
	 * Method that pops up dialog box that notifies the
	 * user about error conditions.
	 */
	public void showError(String theMessage)
	{
		JOptionPane.showMessageDialog(null,theMessage);
		return;
	}

	public AMSMainView getAmsMainView() 
	{
		return amsMainView;
	}
}
