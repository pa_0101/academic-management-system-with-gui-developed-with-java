package ams.view;

import java.awt.*;
import java.awt.event.*;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import ams.controller.InitializeProgramDialogController;

/**
 * @author Paolo Felicelli
 * InitializeProgramDialog class is a popup dialog that accepts 
 * program title and code information.
 */
@SuppressWarnings("serial")
public class InitializeProgramDialog extends JDialog implements ActionListener
{
	private AMSMainView amsMainView;
	private InitializeProgramDialogController programControl;
	private JTextField programCodeText = new JTextField();
	private JTextField programTitleText = new JTextField();
	
	public InitializeProgramDialog(AMSMainView amsMainView)
	{
		this.amsMainView = amsMainView;
		programControl = new InitializeProgramDialogController(this);
		setTitle("Initialize Program");
		setSize(350, 130);
		
		//APPLICATION_MODAL blocks all input to the top level window
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		//set the layout of the dialog box
		GridLayout myGridLayout = new GridLayout(); 
		myGridLayout.setColumns(2);
		myGridLayout.setRows(3);
		
		//instatiate dialog labels and button
		JLabel codeLabel = new JLabel("Program Code");
		JLabel titleLabel = new JLabel("Program Title");
		JButton initializeProgram = new JButton("Initialize Program");
		
		//instantiate panels to contain labels
		JPanel codeLabelPanel = new JPanel();
		JPanel titleLabelPanel = new JPanel();
		
		//instantiate panels to contain text fields
		JPanel codePanel = new JPanel();
		JPanel titlePanel = new JPanel();
		
		//instantiate panel to contain all panels
		JPanel mainPanel = new JPanel();
		mainPanel.setBorder(new EmptyBorder(10,10,10,10));
		
		codePanel.setLayout(new BorderLayout());
		titlePanel.setLayout(new BorderLayout());
		
		//instantiate text fields for text input
		programCodeText = new JTextField();
		programTitleText = new JTextField();
		
		//add text fields to panels
		codePanel.add(programCodeText, BorderLayout.NORTH);
		titlePanel.add(programTitleText, BorderLayout.NORTH);
		
		codeLabelPanel.setLayout(new BorderLayout());
		titleLabelPanel.setLayout(new BorderLayout());
		
		//add labels to panels
		codeLabelPanel.add(codeLabel, BorderLayout.NORTH);
		titleLabelPanel.add(titleLabel, BorderLayout.NORTH);
		
		mainPanel.setLayout(myGridLayout);
		
		//add all panels to the main panel
		mainPanel.add(codeLabelPanel);
		mainPanel.add(codePanel);
		mainPanel.add(titleLabelPanel);
		mainPanel.add(titlePanel);
		mainPanel.add(initializeProgram);
		
		//register the JButton with action listeners.
		initializeProgram.addActionListener(this);
		
		add(mainPanel);
		setLocationRelativeTo(this);
		
		//registerKeyboardAction allows user to interact with JButton using enter/spacebar key
		initializeProgram.registerKeyboardAction(initializeProgram.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		initializeProgram.registerKeyboardAction(initializeProgram.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
}
	/**
	 * ActionPerformed is invoked when init program button is pressed.
	 * Grabs the text input and passes it to initializeProgram as parameters.
	 * Also invokes refreshStatusBar in the main view to update the status bar with 
	 * passed in parameters.
	 */
	public void actionPerformed(ActionEvent arg0) 
	{
		if(arg0.getActionCommand().equalsIgnoreCase("Initialize Program"))
		{
			String programCode = programCodeText.getText();
			String programTitle = programTitleText.getText();
			
			if(programCode.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"You must enter program code");
				return;
			}
			
			if(programCode.length() != 6)
			{
				JOptionPane.showMessageDialog(null,"Program code must 6 alphanumeric characters");
				return;
			}
			
			if(programTitle.length() < 2)
			{
				JOptionPane.showMessageDialog(null,"Minimum length of program title is 2 characters");
				return;
			}
			
			if(programTitle.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"You must enter program title");
				return;
			}
			
			programControl.initializeProgram(programCode, programTitle);
			//setVisible(false) closes the window
			setVisible(false);
			//call to update the status bar with program code and title input
			amsMainView.refreshStatusBar();
			/**
			 * dispose() causes the JDialog window to be destroyed 
			 * and cleaned up by the operating system
			 */
			dispose();
		}
	}
	
	public AMSMainView getAmsMainView() 
	{
		return amsMainView;
	}
}
