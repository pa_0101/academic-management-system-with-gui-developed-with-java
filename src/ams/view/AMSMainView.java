package ams.view;

import java.awt.BorderLayout;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.*;
import ams.model.facade.AMSModel;
import ams.view.grid.Grid;
import ams.view.grid.GridCell;

/**
 * @author Paolo Felicelli
 * AMSMainView is responsible for instantiating and laying 
 * out all the main frame components.
 */
@SuppressWarnings("serial")
public class AMSMainView extends JFrame
{
	private AMSModel model;
	private Menu menu;
	private ToolBar toolBar; 
	private Grid grid;
	private GridCell gridCell;
	private StatusBar statusBar;
	
	public AMSMainView(AMSModel model)
	{
		this.model = model;
		setTitle("Academic Management System");
		setSize(800, 600);
		//center the frame
		setLocationRelativeTo(null); 
		setLayout(new BorderLayout());
		
		/**
		 * Method that prompts the user to confirm exiting the application
		 */
		setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		addWindowListener(new WindowAdapter()
		{
		    public void windowClosing(WindowEvent e)
		    {
		        JFrame frame = (JFrame)e.getSource();
		 
		        int result = JOptionPane.showConfirmDialog(frame,
		            "Are you sure you want to exit the application?",
		            "Exit Application",
		            JOptionPane.YES_NO_OPTION);
		 
		        if(result == JOptionPane.YES_OPTION)
		        {
		            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		        }
		    }
		});

		//set the menu bar
		menu = new Menu(this);
		setJMenuBar(menu);
		
		//layout toolbar
		toolBar = new ToolBar(this);
		add(toolBar, BorderLayout.NORTH);
		
		//layout grid
		grid = new Grid(this);
		add(grid, BorderLayout.CENTER);
		
		//layout status bar
		statusBar = new StatusBar(this);
		add(statusBar, BorderLayout.SOUTH);		
	}

	public AMSModel getModel() 
	{
		return model;
	}
	
	public StatusBar getStatusBar() 
	{
		return statusBar;
	}

	public Grid getGrid()
	{
		return grid;
	}
	
	public GridCell getGridCell()
	{
		return gridCell;
	}
	
	/**
	 * refreshAll method refreshes the grid cells
	 * and the status bar when changes are made.
	 */
	public void refreshAll()
	{	
		//refresh() method call to update the grid when
		//courses are removed/added
		grid.refresh();
		//revalidate is called on a container once new components 
		//are added or old ones removed. This call is an instruction to 
		//tell the layout manager to reset based on the new component list.
		grid.revalidate();
		refreshStatusBar();
		repaint();
	}
	
	/**
	 * refreshStatusBar() method updates the status bar
	 * when the system is initialized with a new program
	 * and or new course added/removed.
	 */
	public void refreshStatusBar()
	{
		String statusText = "PROGRAM - " + model.getProgram().toString() 
				+ " CORE - " + model.countCoreCourses() + " ELECTIVE - " 
				+ model.countElectiveCourses();		
		statusBar.update(statusText);
	}
}
