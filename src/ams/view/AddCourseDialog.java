package ams.view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;

import javax.swing.*;
import javax.swing.border.EmptyBorder;

import ams.controller.AddCourseDialogController;
import ams.model.Course;

/**
* @author Paolo Felicelli
* AddCourseDialog class is a popup dialog that allows the
* user to enter course parameters and select the pre-requisites
* related to the course.
*/
@SuppressWarnings("serial")
public class AddCourseDialog extends JDialog implements ActionListener 
{
	private AMSMainView amsMainView;
	private AddCourseDialogController courseControl;
	private JTextField courseCodeText;
	private JTextField courseTitleText;
	@SuppressWarnings("rawtypes")
	private JList preReqList;
	private JButton addCourse;
	private JRadioButton radioCore;
	private JRadioButton radioElective;
	private JRadioButton radioCredit6;
	private JRadioButton radioCredit12;
	
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	public AddCourseDialog(AMSMainView amsMainView )
	{
		this.amsMainView = amsMainView;
		courseControl = new AddCourseDialogController(this);
		setTitle("Add Course");
		setSize(350, 400);
		
		//APPLICATION_MODAL blocks all input to the top level window
		setModalityType(ModalityType.APPLICATION_MODAL);
		
		//set the layout of the dialog box
		GridLayout myGridLayout = new GridLayout(); 
		myGridLayout.setColumns(2);
		myGridLayout.setRows(6);
		
		//instatiate dialog labels and button
		JLabel codeLabel = new JLabel("Course Code");
		JLabel titleLabel = new JLabel("Course Title");
		JLabel preReqLabel = new JLabel("Pre Requisites");
		addCourse = new JButton("Add Course");	
		
		//instantiate panels to contain labels
		JPanel codeLabelPanel = new JPanel();
		JPanel titleLabelPanel = new JPanel();
		JPanel preReqLabelPanel = new JPanel();
		
		//instantiate panels to contain text fields
		JPanel codePanel = new JPanel();
		JPanel titlePanel = new JPanel();
		JPanel preReqPanel = new JPanel();
		
		//instantiate course radio buttons 
		radioCore = new JRadioButton("Core");
		radioElective = new JRadioButton("Elective");
		radioCore.setSelected(true);
		ButtonGroup buttonGroup = new ButtonGroup();		
        buttonGroup.add(radioCore);		
        buttonGroup.add(radioElective);
	
        //instantiate credit points radio buttons 
        radioCredit6 = new JRadioButton("6 Credits");
		radioCredit12 = new JRadioButton("12 Credits");
		radioCredit12.setSelected(true);
		ButtonGroup buttonGroup2 = new ButtonGroup();		
        buttonGroup2.add(radioCredit6);		
        buttonGroup2.add(radioCredit12);
        
        //instantiate the main panel for the components
		JPanel mainPanel = new JPanel();
		
		//set the gap between the main panels edge and the components
		mainPanel.setBorder(new EmptyBorder(10,10,10,10));
		
		//panel/component layout
		codePanel.setLayout(new BorderLayout());
		titlePanel.setLayout(new BorderLayout());
		preReqPanel.setLayout(new BorderLayout());
		
		//text field for the user to enter course parameters
		courseCodeText = new JTextField();
		courseTitleText = new JTextField();
		
		//all data for view (JList) are stored in DefaultListModel
		DefaultListModel listModel = new DefaultListModel(); 
		
		Course[] myCourses = courseControl.getAllCourses();
		
		if(myCourses != null)
		{		
			for(int i = 0; i < myCourses.length; i++)
			{
				//get the code and title to store in the listModel
				String courseCode = myCourses[i].getCode();
				String courseTitle = myCourses[i].getTitle();
				
				//add the code and title to the listModel
				listModel.addElement(courseCode + " - " + courseTitle);
			}
		}
		
		//add the listModel to JList constructor for display
		preReqList = new JList(listModel);
		
		//add scroll pane to the JList
		JScrollPane scrollPane = new JScrollPane(preReqList);
		
		//layout al the component panels
		codePanel.add(courseCodeText, BorderLayout.NORTH);
		titlePanel.add(courseTitleText, BorderLayout.NORTH);
		
		codeLabelPanel.setLayout(new BorderLayout());
		titleLabelPanel.setLayout(new BorderLayout());
		
		codeLabelPanel.add(codeLabel, BorderLayout.NORTH);
		titleLabelPanel.add(titleLabel, BorderLayout.NORTH);
		
		mainPanel.setLayout(myGridLayout);
		
		//add all the component panels to the main panel
		mainPanel.add(codeLabelPanel);
		mainPanel.add(codePanel);
		mainPanel.add(titleLabelPanel);
		mainPanel.add(titlePanel);
		mainPanel.add(radioCore);
        mainPanel.add(radioElective);
        mainPanel.add(radioCredit6);
        mainPanel.add(radioCredit12);
		mainPanel.add(preReqLabel);
		mainPanel.add(scrollPane);
		mainPanel.add(addCourse);
		addCourse.addActionListener(this);
		add(mainPanel);
		setLocationRelativeTo(this);
		
		//registerKeyboardAction allows user to interact with JButton using enter/spacebar key
		addCourse.registerKeyboardAction(addCourse.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, false)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, false),
                JComponent.WHEN_FOCUSED);
		addCourse.registerKeyboardAction(addCourse.getActionForKeyStroke(
                KeyStroke.getKeyStroke(KeyEvent.VK_SPACE, 0, true)),
                KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true),
                JComponent.WHEN_FOCUSED);
	}
	
	/**
	 * ActionPerformed is invoked when add course button is pressed.
	 * Grabs the text input and passes it to addCourseDialogController as parameters.
	 * Also invokes refreshAll to update the grid 
	 */
	public void actionPerformed(ActionEvent arg0) 
	{
		if(arg0.getActionCommand().equalsIgnoreCase("Add Course"))
		{
			//get the user input text
			String courseCode = courseCodeText.getText();
			String courseTitle = courseTitleText.getText();
			
			//conditional check, flag, for radio button selection
			String courseType = "CORE";
			if(radioElective.isSelected())
			{
				courseType = "ELECTIVE";
			}
			
			int creditPoints = 12;
			if(radioCredit6.isSelected())
			{
				creditPoints = 6;
			}
			
			//get the selected preReqs in the list, dump to array
			Object[] selectedPreReqs = preReqList.getSelectedValuesList().toArray();
			String[] preReqStringArray = null;
			
			if(selectedPreReqs.length > 0)
			{
				preReqStringArray = new String[selectedPreReqs.length];
				
				for(int i = 0; i < selectedPreReqs.length; i++)
				{
					//get representation of the selected pre reqs
					String selectedCourse = selectedPreReqs[i].toString();
					//split the code from the title
					String[] courseCodeParts = selectedCourse.split(" - ");
					String selectedCourseCode = courseCodeParts[0];
					preReqStringArray[i] = selectedCourseCode;
				}
			}
			
			/**
			 * Check for user input
			 */
			if(courseCode.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"You must enter course code");
				return;
			}
			
			if(courseCode.length() != 8)
			{
				JOptionPane.showMessageDialog(null,"Course code must 8 alphanumeric characters");
				return;
			}
			
			if(courseTitle.isEmpty())
			{
				JOptionPane.showMessageDialog(null,"You must enter course title");
				return;
			}
			
			if(!Character.isUpperCase(courseTitle.charAt(0)))
			{
				JOptionPane.showMessageDialog(null,"First course title character must be an upper-case letter");
				return;
			}
			
			if(courseTitle.length() < 2)
			{
				JOptionPane.showMessageDialog(null,"Minimum length of course title is 2 characters");
				return;
			}
			
			if(courseType.equalsIgnoreCase("CORE") && radioCredit6.isSelected())
			{
				JOptionPane.showMessageDialog(null,"Core course must be 12 credit points");
				return;
			}
			
			//delegate all addCourse parameters to the addCourseDialogController
			courseControl.addCourse(courseCode, courseTitle, preReqStringArray, courseType, creditPoints);
			setVisible(false);
			//refreshAll() call to update the grid
			amsMainView.refreshAll();
			/**
			 * dispose() causes the JDialog window to be destroyed 
			 * and cleaned up by the operating system
			 */
			dispose();
		}
	}
	
	public AMSMainView getAmsMainView() 
	{
		return amsMainView;
	}
}
